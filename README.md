# Projet fil rouge CESI BREST

*CAM : Équipe projet fil rouge composé de Clément DEBOOS, Max-André LEROY et Adrien FERNANDES*

## Live preview
    https://madera-8ef40.firebaseapp.com/

## Installation

    git clone https://github.com/Untel/madera
    cd madera
    npm i -g @angular/cli ts-node
    npm i
    ng serve -o
 
## Déploiement

    cd madera
    npm i -g firebase-tools


    npm run deploy